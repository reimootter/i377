package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Table(name = "order_rows")
public class OrderRow {
    @Column(name = "item_name")
    private String itemName;
    @Min(1)
    private int quantity;
    @NotNull
    @Min(1)
    private Integer price;
}
