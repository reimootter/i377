package dao;

import conf.DbConfig;
import model.Order;
import model.OrderRow;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args)  {

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);

        Order order = new Order();
        order.setOrderNumber("A123");
        order.getOrderRows().add(new OrderRow("pesumasin",1,300));
        order.getOrderRows().add(new OrderRow("telefon",1,400));
        //dao.save(order);
        System.out.println(dao.save(order));
        //System.out.println(dao.findById(1L));
        //dao.deleteAll();
        System.out.println(dao.findAll());
        //dao.getReport();
        ctx.close();
    }
}
