package dao;

import model.Order;
import model.Report;
import java.util.List;

public interface OrderDao {

    Order save(Order order);

    List<Order> findAll();

    void deleteById(Long id);

    void deleteAll();

    Order findById(Long id);

    Report getReport();
}
