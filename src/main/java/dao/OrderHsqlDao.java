package dao;

import model.Order;
import model.OrderRow;
import model.Report;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;


@Repository
public class OrderHsqlDao implements OrderDao {


    @PersistenceContext
    private EntityManager em;


    @Transactional
    public Order save(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        }else{
            em.merge(order);
        }
        return order;
    }


    public List<Order> findAll() {

        return em.createQuery(
                "select o from Order o",
                Order.class).getResultList();

    }


    public Order findById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class);

        query.setParameter("id", id);
        return query.getSingleResult();
    }


    @Transactional
    public void deleteById(Long id){
        Order order =em.find(Order.class, id);
        if(order != null) em.remove(order);
    }


    @Transactional
    public void deleteAll(){
        Query query = em.createQuery("delete from Order");
        query.executeUpdate();
    }

    @Override
    public Report getReport() {
        Report report = new Report();
        report.setCount(findAll().size());
        report.setAverageOrderAmount(sum() / findAll().size());
        report.setTurnoverWithoutVAT(sum());
        report.setTurnoverVAT(sum() * 1/5);
        report.setTurnoverWithVAT(sum() * 1/5 + sum());
        return report;
    }

    private int sum() {

        List<Order> orders = findAll();
        int amount = 0;
        for (Order order : orders) {
            List<OrderRow> orderRows = order.getOrderRows();

            for (OrderRow orderRow : orderRows) {
                int quantity = orderRow.getQuantity();
                int price = orderRow.getPrice();
                int priceSum = price * quantity;
                amount = amount + priceSum;
            }
        }
        return amount;
    }
}